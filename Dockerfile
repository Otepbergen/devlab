# Используйте официальный образ Python в качестве базового образа
FROM python:3.8

# Установите переменные среды
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Установите рабочий каталог внутри контейнера
WORKDIR /app

# Копируйте файлы зависимостей (requirements.txt) в контейнер
COPY requirements.txt /app/

# Установите зависимости
RUN pip install -r requirements.txt

# Копируйте все файлы вашего проекта в контейнер
COPY . /app/

# Запустите команду для выполнения миграций и запуска проекта
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
